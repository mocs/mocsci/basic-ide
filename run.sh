#!/usr/bin/env bash

# Use this file to start the build.

# Always die on error
set -e

# Get the folder name of _this_ script
SRCDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
# Get the folder of the executing instance
TARGET=$(pwd)
MANIFEST=MANIFEST

# Get common stuff
source ${SRCDIR}/scripts/common/shell.sh

# Load help_text
source help_text

#Check arguments
while [[ $# -gt 0 ]]; do
  key="$1"
  case $key in
    -d|--debug)
      DEBUG=1
      printf "${CYAN}"
      printf "Running in debug mode.\n"
      shift
      ;;
    -h|--help)
      printf "${NC}"
      printf "${_help_text}"
      exit
    ;;
    -m|--manifest)
      MANIFEST="$2"
      shift
      shift
    ;;
    *)
      printf "${CYAN}"
      printf "Unknown command given. (${1})\n"
      printf "${RED}"
      printf "Exiting.\n"
      printf "${NC}"
      printf "${_help_text}"
      exit
  esac
done
printf "${CYAN}"
printf "Using manifest file: ${BLUE}${MANIFEST}${NC}\n"

# Sanity checks
printf "${MAGENTA}"
printf "###############################################################################\n"
printf "### Executing sanity checks!                                                ###\n"
printf "###############################################################################\n"
printf "${NC}"
printf "${cyan}"
printf "     Checking that we have a ${MANIFEST}..."
if ! [[ -f ${MANIFEST} ]]; then
  printf "\r"
  _exit_cmd "Could not find MANIFEST in ${TARGET}." 1
else
  printf "\r${GREEN}OK\n${NC}"
fi
source ${MANIFEST}
IMG_PARTS=($(echo "${IMG}" | tr '/' '\n'))
printf "${cyan}"
printf "     Checking that we have a valid IMG..."
if [ ${#IMG_PARTS[@]} -le 1 ]; then
  printf "\r"
  _exit_cmd "IMG must have at least one '/'." 1
else
  printf "\r${GREEN}OK\n${NC}"
fi
printf "${cyan}"
printf "     Checking that we have a valid TAG..."
if [[ "${TAG}" =~ '([a-zA-Z0-9_-]*[\.]*' ]]; then
  printf "\r"
  _exit_cmd "TAG must only contain [a-zA-Z0-9\.-_]." 1
else
  printf "\r${GREEN}OK\n${NC}"
fi

# Run the program
printf "${MAGENTA}"
printf "###############################################################################\n"
printf "### Starting basic-ide!                                                     ###\n"
printf "###############################################################################\n"
printf "${NC}"
START=$(date +%s.%N | tr 'N' '0')
# Create a random name to allow cleanup later.
container_name=$(random_string)
# Get the local ip
local_ip=$(get_local_ip)
# Define certificate variables
if [[ -z $CERT_C ]]; then
  CERT_C='SE'
fi
if [[ -z $CERT_ST ]]; then
  CERT_ST=${container_name}
fi
if [[ -z $CERT_L ]]; then
  CERT_L="${IMG_PARTS[0]}"
fi
if [[ -z $CERT_O ]]; then
  CERT_O="${IMG_PARTS[1]}"
fi
if [[ -z $CERT_OU ]]; then
  CERT_OU=${TAG}
fi
if [[ -z $CERT_EMAIL ]]; then
  for part in ${IMG_PARTS[@]}; do
    if [[ -z $CERT_EMAIL ]]; then
      CERT_EMAIL="${TAG}@${part}"
    else
      CERT_EMAIL+=".${part}"
    fi
  done
fi

# Create a one time password
otp=$(random_string)
# Share the otp and other secrets by creating a file that is mounted. Should(?)
# be more secure than just using environmental variables.
mkdir -p ~/.basic-ide-secrets
echo "$otp" > ~/.basic-ide-secrets/otp
echo "$local_ip" > ~/.basic-ide-secrets/ip
echo "$CERT_C" > ~/.basic-ide-secrets/cert_c
echo "$CERT_ST" > ~/.basic-ide-secrets/cert_st
echo "$CERT_L" > ~/.basic-ide-secrets/cert_l
echo "$CERT_O" > ~/.basic-ide-secrets/cert_o
echo "$CERT_OU" > ~/.basic-ide-secrets/cert_ou
echo "$CERT_EMAIL" > ~/.basic-ide-secrets/cert_email
# Start the container in daemon mode to be able to get data
id=$(${docker_cmd} run -d -P --name "${container_name}" -v ~/.basic-ide-secrets:/.basic-ide-secrets:ro "${IMG}":"${TAG}")
# Get the port
port=$(${docker_cmd} ps --filter name=${container_name} --format "{{.Ports}}" | awk -F'[:-]' '{print $2}')
# Give user information!
printf "Connect to:\n"
printf "${MAGENTA}https://$local_ip:$port/vnc.html?resize=remote&password=$otp\n"
printf "${CYAN}"
printf "${BLUE}Verify${CYAN} that the ${BLUE}certificate${CYAN} contains the following:\n"
printf "${YELLOW}            Country = ${GREEN}${CERT_C}\n"
printf "${YELLOW}              State = ${GREEN}${CERT_ST}\n"
printf "${YELLOW}           Locality = ${GREEN}${CERT_L}\n"
printf "${YELLOW}       Organization = ${GREEN}${CERT_O}\n"
printf "${YELLOW}Organizational Unit = ${GREEN}${CERT_OU}\n"
printf "${YELLOW}        Common Name = ${GREEN}${local_ip}\n"
printf "${YELLOW}      Email Address = ${GREEN}${CERT_EMAIL}\n"
printf "${NC}"
# Override the local exit_handler
function local_exit_handler() {
  code=$?
  printf "\n${BLUE}"
  printf "Exit condition fulfilled.\n"
  printf "${NC}"
  printf "Shutting down...\n"

  STOP=$(date +%s.%N | tr 'N' '0')
  DIFF=$(echo "$STOP - $START" | bc)
  TIME=$(format_time "${DIFF}")
  printf "${CYAN}"
  printf "Done in ${TIME}.\n\n"

  if ! [[ -z $DEBUG ]]; then
    printf "${MAGENTA}"
    printf "###############################################################################\n"
    printf "### Debug requested. Printing logs...                                       ###\n"
    printf "###############################################################################\n"
    printf "${NC}"
    printf "${yellow}"
    ${docker_cmd} logs "${id}"
    printf "${NC}"
  fi

  printf "${MAGENTA}"
  printf "###############################################################################\n"
  printf "### Cleaning                                                                ###\n"
  printf "###############################################################################\n"
  printf "${NC}"
  printf "${cyan}"
  printf "Stopping container...\n"
  printf "${NC}"
  ${docker_cmd} stop "${container_name}"
  sleep 3
  printf "${cyan}"
  printf "Removing stopped container...\n"
  printf "${NC}"
  ${docker_cmd} rm "${container_name}"
  printf "${cyan}"
  printf "Cleaning otp...\n"
  printf "${NC}"
  rm -fr ~/.basic-ide-secrets

  #Exit
  printf "${MAGENTA}"
  printf "###############################################################################\n"
  printf "### All done!                                                               ###\n"
  printf "###############################################################################\n"
  printf "${NC}"
  exit $code
}
trap local_exit_handler EXIT
function ctrl_c() {
  exit 0
}
trap ctrl_c SIGINT SIGTERM

# Create a countdown loop. Let's make it 2 minutes. The exit will be done from
# the entrypoint though, this is just for show.
printf "${CYAN}NOTE: ${cyan}If you do not login into the container within 2 minutes it will exit for security reasons.\n"
for (( i = 120; i > 0; i-- )); do
  printf "\r${YELLOW}${i} s "
  $(${docker_cmd} inspect -f '{{.State.Running}}' ${container_name})
  sleep 1
done
printf "\r${YELLOW}0 s\n${NC}"
# Create an infinite loop until container is exited or Ctrl-C is pressed!
count=0
while true; do
  case $count in
    0)
      printf "\r/"
      ;;
    1)
      printf "\r-"
      ;;
    2)
      printf "\r\\"
      ;;
    3)
      printf "\r|"
      count=0
      ;;
  esac
  count=$(($count+1))
  $(${docker_cmd} inspect -f '{{.State.Running}}' ${container_name})
  sleep 1
done
