# Basic IDE

Basic IDE container image that uses a minimal centos image and adds tigervnc and novnc to provide a desktop capable of running on a kubernetes cluster.

# Instructions

## Patching
Since busybox is used to minimize image size, patches must be created like this:

1. Copy the original file, e.g., `cp launch.sh launch.orig`
2. Change stuff.
3. Create patchfile, e.g., `diff -Nar -U 5 launch.sh.orig launch.sh > ./src/patches/000-launch.sh.patch`
