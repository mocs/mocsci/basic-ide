#!/usr/bin/env bash

# This file acts as an entrypoint for the container.

# Always die on error
set -e

# Get common stuff
source /bin/shell.sh

# Get the one time password and set it
otp=$(cat /.basic-ide-secrets/otp)
mkdir -p "$HOME/.vnc"
PASSWD_PATH="$HOME/.vnc/passwd"
echo "$otp" | vncpasswd -f >> $PASSWD_PATH
chmod 600 $PASSWD_PATH

# Create a self signed certificate
IP=$(cat /.basic-ide-secrets/ip)
CERT_C=$(cat /.basic-ide-secrets/cert_c)
CERT_ST=$(cat /.basic-ide-secrets/cert_st)
CERT_L=$(cat /.basic-ide-secrets/cert_l)
CERT_O=$(cat /.basic-ide-secrets/cert_o)
CERT_OU=$(cat /.basic-ide-secrets/cert_ou)
CERT_EMAIL=$(cat /.basic-ide-secrets/cert_email)
openssl req -new -x509 -days 5 -nodes -out $NOVNC_HOME/utils/self.pem -keyout $NOVNC_HOME/utils/self.pem \
  -subj "/C=${CERT_C}/ST=${CERT_ST}/L=${CERT_L}/O=${CERT_O}/OU=${CERT_OU}/CN=${IP}/emailAddress=${CERT_EMAIL}"

# Start noVNC
$NOVNC_HOME/utils/launch.sh --ssl-only --run-once --vnc localhost:5901 --listen 6901 &
PID_NOVNC=$!

# Start Xvnc
vncserver $DISPLAY -depth 24 -geometry 1280x1024 &
PID_XVNC=$!

# Start fvwm
# Disable screensaver and power management
# Wait for Xvnc to start before launching
# TODO: Better way of detecting that the server has started needed. Sleep is
# fragile.
sleep 2
xset -dpms
xset s noblank
xset s off

/usr/bin/fvwm &
PID_FVWM=$!

# Allow noVNC to connect to Xvnc
xhost 127.0.0.1

# Set background
fvwm-root /headless/.fvwm/background.png

# Set a two minute timeout for connecting.
printf "Starting two minute connection timeout...\n"
TIMEOUT=1
for (( i = 120; i > 0; i-- )); do
  set +e
  nothing=$(grep "Connections:\ accepted:" ${HOME}/.vnc/*.log)
  ec=$?
  set -e
  if [[ $ec -eq 0 ]]; then
    printf "Connection established!\n"
    TIMEOUT=0
    break
  fi
  if [[ $i -eq 60 ]]; then
    printf "60 s remaining.\n"
  elif [[ $i -eq 30 ]]; then
    printf "30 s remaining.\n"
  elif [[ $i -eq 10 ]]; then
    printf "10 s remaining.\n"
  elif [[ $i -eq 5 ]]; then
    printf "5 s remaining.\n"
  fi
  sleep 1
done
if [[ $TIMEOUT -eq 0 ]]; then
  # Wait for fvwm to die
  printf "Waiting for fvwm to exit!\n"
  wait $PID_FVWM
else
  printf "Timed out! Exiting!\n"
fi
