# This Dockerfile is used to build an headless vnc image based on Centos

############################################################
# Install tigervnc and novnc
############################################################
ARG arch
ARG ver
FROM registry.gitlab.com/mocs/mocsci/centos-minimalist-image/centos-yum:${ver}_${arch} as builder
ARG arch

# Set the target folder
ARG target_root=/target_image/rootfs

# Allow epel repo
RUN set -eux; yum -y \
  --setopt=tsflags='nodocs' \
  --releasever=7 \
  --setopt=override_install_langs=en_US.utf8 install \
  epel-release

# Install packages
RUN set -eux; yum -y \
  --setopt=tsflags='nodocs' \
  --releasever=7 \
  --setopt=override_install_langs=en_US.utf8 install \
  tigervnc-server \
  fvwm \
  python3 \
  python36-numpy \
  pyOpenSSL \
  openssl \
  xorg-x11-fonts-100dpi

# Install copy libs script
COPY ./scripts/common/*.sh /usr/bin/
COPY ./scripts/bin/*.sh /usr/bin/

# Copy Xvnc
RUN mkdir -p ${target_root}/usr/bin \
  && cp /usr/bin/Xvnc ${target_root}/usr/bin/ \
  && copy_dynamic_libs.sh /usr/bin/Xvnc ${target_root}
RUN mkdir -p ${target_root}/usr/bin \
  && cp /usr/bin/vncpasswd ${target_root}/usr/bin/ \
  && copy_dynamic_libs.sh /usr/bin/vncpasswd ${target_root}
RUN mkdir -p ${target_root}/usr/bin \
  && cp /usr/bin/vncserver ${target_root}/usr/bin/
# vncserver is a perl script! Bring perl!
# NOTE: We should probably create some kind of loading function to minimize
# the perl modules. Currently, everything is copied.
RUN mkdir -p ${target_root}/usr/bin \
  && cp /usr/bin/perl ${target_root}/usr/bin/ \
  && copy_dynamic_libs.sh /usr/bin/perl ${target_root}
RUN mkdir -p ${target_root}/usr/share/perl5 \
  && cp -r /usr/share/perl5/* ${target_root}/usr/share/perl5/
RUN mkdir -p ${target_root}/usr/lib64/perl5 \
  && cp -r /usr/lib64/perl5/* ${target_root}/usr/lib64/perl5/

# Copy all X-stuff
RUN mkdir -p ${target_root}/usr/bin \
  && cp /usr/bin/xauth ${target_root}/usr/bin/ \
  && copy_dynamic_libs.sh /usr/bin/xauth ${target_root} \
  && mkdir -p ${target_root}/usr/bin \
  && cp /usr/bin/xhost ${target_root}/usr/bin/ \
  && copy_dynamic_libs.sh /usr/bin/xhost ${target_root} \
  && mkdir -p ${target_root}/usr/share \
  && cp -r /usr/share/X11 ${target_root}/usr/share/ \
  && cp /usr/bin/setxkbmap ${target_root}/usr/bin/ \
  && copy_dynamic_libs.sh /usr/bin/setxkbmap ${target_root} \
  && cp /usr/bin/xkbcomp ${target_root}/usr/bin/ \
  && copy_dynamic_libs.sh /usr/bin/xkbcomp ${target_root} \
  && cp /usr/bin/xset ${target_root}/usr/bin/ \
  && copy_dynamic_libs.sh /usr/bin/xset ${target_root} \
  && cp /usr/bin/xsetroot ${target_root}/usr/bin/ \
  && copy_dynamic_libs.sh /usr/bin/xsetroot ${target_root} \
  && mkdir -p ${target_root}/etc/fonts \
  && cp /etc/fonts/fonts.conf ${target_root}/etc/fonts \
  && cp /usr/bin/mkfontdir ${target_root}/usr/bin/ \
  && cp /usr/bin/mkfontscale ${target_root}/usr/bin/ \
  && mkdir -p ${target_root}/usr/share/ \
  && cp -r /usr/share/fonts ${target_root}/usr/share \
  && mkdir -p ${target_root}/usr/lib/ \
  && cp -r /usr/lib/fontconfig ${target_root}/usr/lib/ \
  && mkdir -p ${target_root}/usr/bin \
  && cp /usr/bin/fc-cache ${target_root}/usr/bin/ \
  && cp /usr/bin/xorg-x11-fonts-update-dirs ${target_root}/usr/bin/


# Copy python which is needed by websockify
# NOTE: websockify has a c implementation that we should use (if it works.)
# That should make a much smaller footprint and be faster.
RUN # Install packages
RUN set -eux; yum -y \
    --setopt=tsflags='nodocs' \
    --releasever=7 \
    --setopt=override_install_langs=en_US.utf8 install \
    gcc \
    python3-devel \
    && pip3 install websocket
RUN mkdir -p ${target_root}/usr/bin \
  && cp /usr/bin/python3 ${target_root}/usr/bin/python \
  && copy_dynamic_libs.sh /usr/bin/python3 ${target_root}
RUN mkdir -p ${target_root}/usr/lib64 \
  && cp -r /usr/lib64/python3.6 ${target_root}/usr/lib64/ \
  && mkdir -p ${target_root}/usr/local/lib \
  && cp -r /usr/local/lib/python3.6 ${target_root}/usr/local/lib/
# This is requiring an awful lot of manual steps...
RUN mkdir -p ${target_root}/usr/lib64 \
  && cp /usr/lib64/libssl.so.10 ${target_root}/usr/lib64/ \
  && copy_dynamic_libs.sh /usr/lib64/libssl.so.10 ${target_root} \
  && cp /usr/lib64/libopenblasp.so.0 ${target_root}/usr/lib64/ \
  && copy_dynamic_libs.sh /usr/lib64/libopenblasp.so.0 ${target_root}
RUN mkdir -p ${target_root}/usr/bin \
  && cp /usr/bin/ps ${target_root}/usr/bin/ \
  && copy_dynamic_libs.sh /usr/bin/ps ${target_root} \
  && copy_dynamic_libs.sh /usr/lib64/python3.6/site-packages/numpy/random/mtrand.cpython-36m-x86_64-linux-gnu.so ${target_root} \
  && copy_dynamic_libs.sh /usr/lib64/python3.6/site-packages/numpy/fft/fftpack_lite.cpython-36m-x86_64-linux-gnu.so ${target_root} \
  && copy_dynamic_libs.sh /usr/lib64/python3.6/site-packages/numpy/core/multiarray_tests.cpython-36m-x86_64-linux-gnu.so ${target_root} \
  && copy_dynamic_libs.sh /usr/lib64/python3.6/site-packages/numpy/core/struct_ufunc_test.cpython-36m-x86_64-linux-gnu.so ${target_root} \
  && copy_dynamic_libs.sh /usr/lib64/python3.6/site-packages/numpy/core/test_rational.cpython-36m-x86_64-linux-gnu.so ${target_root} \
  && copy_dynamic_libs.sh /usr/lib64/python3.6/site-packages/numpy/core/umath.cpython-36m-x86_64-linux-gnu.so ${target_root} \
  && copy_dynamic_libs.sh /usr/lib64/python3.6/site-packages/numpy/core/multiarray.cpython-36m-x86_64-linux-gnu.so ${target_root} \
  && copy_dynamic_libs.sh /usr/lib64/python3.6/site-packages/numpy/core/_dummy.cpython-36m-x86_64-linux-gnu.so ${target_root} \
  && copy_dynamic_libs.sh /usr/lib64/python3.6/site-packages/numpy/core/operand_flag_tests.cpython-36m-x86_64-linux-gnu.so ${target_root} \
  && copy_dynamic_libs.sh /usr/lib64/python3.6/site-packages/numpy/core/umath_tests.cpython-36m-x86_64-linux-gnu.so ${target_root} \
  && copy_dynamic_libs.sh /usr/lib64/python3.6/site-packages/numpy/linalg/lapack_lite.cpython-36m-x86_64-linux-gnu.so ${target_root} \
  && copy_dynamic_libs.sh /usr/lib64/python3.6/site-packages/numpy/linalg/_umath_linalg.cpython-36m-x86_64-linux-gnu.so ${target_root}

# Copy fvwm
RUN mkdir -p ${target_root}/usr/bin \
  && cp /usr/bin/fvwm ${target_root}/usr/bin/ \
  && copy_dynamic_libs.sh /usr/bin/fvwm ${target_root} \
  && mkdir -p ${target_root}/usr/share \
  && cp -r /usr/share/fvwm ${target_root}/usr/share \
  && cp /usr/bin/fvwm-root ${target_root}/usr/bin/ \
  && copy_dynamic_libs.sh /usr/bin/fvwm-root ${target_root} \
  && mkdir -p ${target_root}/usr/libexec/fvwm \
  && cp -r /usr/libexec/fvwm/2.6.6 ${target_root}/usr/libexec/fvwm

# Setup fvwm themes
COPY ./src/blobs/fvwm/fvwm-min-master.zip /tmp/
WORKDIR /tmp
RUN unzip fvwm-min-master.zip \
  && cd fvwm-min-master \
  && sh build | tee build.lst \
  && mkdir -p ${target_root}/usr/share/fvwm/.fvwm/styles/base \
  && mkdir -p ${target_root}/usr/share/fvwm/.fvwm/styles/user \
  && cp config ${target_root}/usr/share/fvwm/.fvwm/ \
  && cp styles/base/* ${target_root}/usr/share/fvwm/.fvwm/styles/base

# Add openssl files
RUN mkdir -p ${target_root}/usr/bin \
  && cp /usr/bin/openssl ${target_root}/usr/bin \
  && copy_dynamic_libs.sh /usr/bin/openssl ${target_root} \
  && mkdir -p ${target_root}/etc/pki/tls/ \
  && cp -r /etc/pki/tls/* ${target_root}/etc/pki/tls/

# Show user what got built!
#RUN tree ${target_root}
